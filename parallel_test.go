package parallel

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/flimzy/testy"
)

func TestGroup(t *testing.T) {
	type tst struct {
		ctx   context.Context
		limit uint
		funcs []Func
		err   string
	}
	tests := testy.NewTable()
	tests.Add("no functions", tst{
		ctx: context.TODO(),
	})
	tests.Add("single function, no error", tst{
		ctx: context.TODO(),
		funcs: []Func{
			func(_ context.Context) error { return nil },
		},
	})
	tests.Add("single function, with error", tst{
		ctx: context.TODO(),
		funcs: []Func{
			func(_ context.Context) error { return errors.New("foo") },
		},
		err: "foo",
	})
	tests.Add("two funcs, one error", tst{
		ctx: context.TODO(),
		funcs: []Func{
			func(_ context.Context) error { return errors.New("bar") },
			func(_ context.Context) error { return nil },
		},
		err: "bar",
	})
	tests.Add("two funcs, last error", tst{
		ctx: context.TODO(),
		funcs: []Func{
			func(_ context.Context) error {
				time.Sleep(50 * time.Millisecond)
				return errors.New("bar")
			},
			func(_ context.Context) error { return nil },
		},
		err: "bar",
	})
	tests.Add("three funcs, second error", tst{
		ctx: context.TODO(),
		funcs: []Func{
			func(_ context.Context) error { return nil },
			func(_ context.Context) error {
				time.Sleep(50 * time.Millisecond)
				return errors.New("bar")
			},
			func(_ context.Context) error { return nil },
		},
		err: "bar",
	})
	tests.Add("canceled context", tst{
		ctx: func() context.Context {
			ctx, cancel := context.WithCancel(context.Background())
			cancel()
			return ctx
		}(),
		funcs: []Func{
			func(_ context.Context) error { return errors.New("bar") },
		},
		err: "context canceled",
	})
	tests.Add("error leads to cancelation", tst{
		ctx: context.Background(),
		funcs: []Func{
			func(_ context.Context) error { return errors.New("bar") },
			func(ctx context.Context) error {
				time.Sleep(50 * time.Millisecond)
				testy.Error(t, "context canceled", ctx.Err())
				return nil
			},
		},
		err: "bar",
	})
	tests.Add("three funcs, limit 2", func(t *testing.T) interface{} {
		var mu sync.Mutex
		var start time.Time

		return tst{
			ctx: context.TODO(),
			funcs: []Func{
				func(_ context.Context) error {
					mu.Lock()
					start = time.Now()
					mu.Unlock()
					time.Sleep(50 * time.Millisecond)
					return nil
				},
				func(_ context.Context) error {
					time.Sleep(50 * time.Millisecond)
					return nil
				},
				func(_ context.Context) error {
					mu.Lock()
					delay := time.Since(start)
					mu.Unlock()
					if delay < 40*time.Millisecond {
						return fmt.Errorf("Only %fs delay", delay.Seconds())
					}
					return nil
				},
			},
			limit: 2,
		}
	})
	tests.Add("panic", tst{
		ctx: context.Background(),
		funcs: []Func{
			func(_ context.Context) error { panic("oink") },
			func(ctx context.Context) error {
				time.Sleep(50 * time.Millisecond)
				testy.Error(t, "context canceled", ctx.Err())
				return nil
			},
		},
		err: "RECOVERED: oink",
	})

	tests.Run(t, func(t *testing.T, test tst) {
		g := Limit(test.ctx, test.limit)
		for _, f := range test.funcs {
			g.Go(f)
		}
		err := g.Wait()
		testy.Error(t, test.err, err)
	})
}
