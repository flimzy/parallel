// Deprecated: Use golang.org/x/sync/errgroup instead.
package parallel

import (
	"context"
	"sync"

	"github.com/pkg/errors"
	"golang.org/x/sync/semaphore"
)

// Func is a function to be run in a parallel group.
type Func func(context.Context) error

// Group manages a group of one or more functions to be executed in parallel.
type Group interface {
	// Go executes f in a goroutine. f may not be called at all, if the group
	// or context has already been cancelled.
	Go(f Func)

	// Wait waits for all group goroutines to exit, or for the first error. It
	// returns the first error, if any occurs. The behavior of a Group after
	// Wait() is first called is undefined, and should be avoided.
	Wait() error
}

type group struct {
	ctx    context.Context
	sem    *semaphore.Weighted
	wg     *sync.WaitGroup
	cancel func()
	err    error
	errMu  sync.RWMutex
}

// New instantiate a new parallel group.
func New(ctx context.Context) Group {
	return Limit(ctx, 0)
}

func Limit(ctx context.Context, limit uint) Group {
	ctx, cancel := context.WithCancel(ctx)
	g := &group{
		ctx:    ctx,
		cancel: cancel,
		wg:     &sync.WaitGroup{},
	}
	if limit > 0 {
		g.sem = semaphore.NewWeighted(int64(limit))
	}
	return g
}

func (g *group) setErr(err error) {
	if err == nil {
		return
	}
	g.errMu.Lock()
	defer g.errMu.Unlock()
	g.cancel()
	if g.err == nil {
		g.err = err
	}
}

func (g *group) Go(f Func) {
	if err := g.ctx.Err(); err != nil {
		g.setErr(err)
		return
	}
	if g.sem != nil {
		if err := g.sem.Acquire(g.ctx, 1); err != nil {
			g.setErr(err)
			return
		}
	}
	g.wg.Add(1)
	go func() {
		defer g.wg.Done()
		defer func() {
			if r := recover(); r != nil {
				err := errors.Errorf("RECOVERED: %v", r)
				g.cancel()
				g.errMu.Lock()
				defer g.errMu.Unlock()
				g.err = err
			}
		}()
		if g.sem != nil {
			defer g.sem.Release(1)
		}
		if err := f(g.ctx); err != nil {
			g.setErr(err)
		}
	}()
}

func (g *group) Wait() error {
	g.wg.Wait()
	g.errMu.RLock()
	defer g.errMu.RUnlock()
	return g.err
}

// GoAll groups New(), Go(), and Wait() into a single call. It executes each fn
// in parallel, and returns the first error to occur.
func GoAll(ctx context.Context, fn ...Func) error {
	group := New(ctx)
	for _, f := range fn {
		group.Go(f)
	}
	return group.Wait()
}
